package com.example.logisticahumanitaria;

import java.io.Serializable;

public class Estados implements Serializable {
    private int _ID;
    private String nombre;
    private String tamanioPoblacion;
    private String altitud;
    private String municipio;
    private String capital;

    public Estados () {
        this._ID = 0;
        this.nombre = "";
        this.tamanioPoblacion = "";
        this.altitud = "";
        this.municipio = "";
        this.capital = "";
    }

    public Estados(int _ID, String nombre, String tamanioPoblacion, String altitud, String municipio,
                   String capital){
        this._ID = _ID;
        this.nombre = nombre;
        this.tamanioPoblacion = tamanioPoblacion;
        this.altitud = altitud;
        this.municipio = municipio;
        this.capital = capital;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamanioPoblacion() {
        return tamanioPoblacion;
    }

    public void setTamanioPoblacion(String tamanioPoblacion) {
        this.tamanioPoblacion = tamanioPoblacion;
    }

    public String getAltitud() {
        return altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}
