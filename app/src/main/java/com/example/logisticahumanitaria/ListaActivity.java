package com.example.logisticahumanitaria;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.logisticahumanitaria.Estados;
import com.example.logisticahumanitaria.R;

import java.io.Serializable;
import java.util.ArrayList;
import database.DbEstados;

public class ListaActivity extends android.app.ListActivity {
    private DbEstados dbEstados;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        dbEstados = new DbEstados(this);
        dbEstados.openDatabase();
        ArrayList<Estados> estados = (ArrayList<Estados>) dbEstados.allEstados();
        MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.list_view, estados);
        setListAdapter(adapter);
        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblEstado = (TextView)view.findViewById(R.id.lblestado);
            TextView lblCapital = (TextView)view.findViewById(R.id.lblcapital);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblEstado.setText(objects.get(position).getNombre());
            lblCapital.setText(objects.get(position).getCapital());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbEstados.openDatabase();
                    dbEstados.deleteEstado(objects.get(position).get_ID());
                    dbEstados.close();
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estado", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }
}