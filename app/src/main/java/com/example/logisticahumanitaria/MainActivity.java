package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.DbEstados;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;
    private EditText edtTamanioPoblacion;
    private EditText edtAltitud;
    private EditText edtMunicipio;
    private EditText edtCapital;
    private Button btnGuardar;
    private Button btnVer;
    private Estados savedEstado;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtNombre = (EditText) findViewById(R.id.nombre);
        edtTamanioPoblacion = (EditText) findViewById(R.id.tpobla);
        edtAltitud = (EditText) findViewById(R.id.altitud);
        edtMunicipio = (EditText) findViewById(R.id.municipio);
        edtCapital = (EditText) findViewById(R.id.capital);
        btnGuardar = (Button) findViewById(R.id.btnguardar);
        btnVer = (Button) findViewById(R.id.btnver);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText[] editTexts = {edtNombre, edtTamanioPoblacion, edtAltitud, edtMunicipio, edtCapital};
                boolean vacio = false;
                for (int x=0; x<editTexts.length && !vacio; x++){
                    if(editTexts[x].getText().toString().trim().matches("")){
                        vacio = true;
                        Toast.makeText(getApplicationContext(),"Llene todos los campos", Toast.LENGTH_SHORT).show();
                    }
                }
                if(!vacio) {
                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDatabase();
                    Estados nEstado = new Estados();
                    nEstado.setNombre(edtNombre.getText().toString());
                    nEstado.setTamanioPoblacion(edtTamanioPoblacion.getText().toString());
                    nEstado.setAltitud(edtAltitud.getText().toString());
                    nEstado.setMunicipio(edtMunicipio.getText().toString());
                    nEstado.setCapital(edtCapital.getText().toString());

                    if (savedEstado == null) {
                        source.insertEstado(nEstado);
                        Toast.makeText(MainActivity.this,"Se guardó el estado correctamente", Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateEstado(nEstado,id);
                        Toast.makeText(MainActivity.this, "Se actualizó el estado correctamente", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                }
            }
        });

        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(i, 0);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
            edtTamanioPoblacion.setText(estado.getTamanioPoblacion());
            edtAltitud.setText(estado.getAltitud());
            edtMunicipio.setText(estado.getMunicipio());
            edtCapital.setText(estado.getCapital());
        }else{
            limpiar();
        }
    }
    public void limpiar(){
        edtNombre.setText("");
        edtTamanioPoblacion.setText("");
        edtAltitud.setText("");
        edtMunicipio.setText("");
        edtCapital.setText("");
    }
}