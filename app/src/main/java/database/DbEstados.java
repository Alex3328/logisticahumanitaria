package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.logisticahumanitaria.Estados;

import java.util.ArrayList;

public class DbEstados {
    Context context;
    EstadoHelper mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.estados._ID,
            DefinirTabla.estados.COLUMN_NAME_ESTADO,
            DefinirTabla.estados.COLUMN_NAME_POBLACION,
            DefinirTabla.estados.COLUMN_NAME_ALTITUD,
            DefinirTabla.estados.COLUMN_NAME_MUNICIPIO,
            DefinirTabla.estados.COLUMN_NAME_CAPITAL
    };
    public DbEstados(Context context){
        this.context = context;
        mDbHelper = new EstadoHelper(this.context);
    }
    public void openDatabase(){
        db = mDbHelper.getWritableDatabase();
    }
    public long insertEstado(Estados c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.estados.COLUMN_NAME_ESTADO, c.getNombre());
        values.put(DefinirTabla.estados.COLUMN_NAME_POBLACION, c.getTamanioPoblacion());
        values.put(DefinirTabla.estados.COLUMN_NAME_ALTITUD, c.getAltitud());
        values.put(DefinirTabla.estados.COLUMN_NAME_MUNICIPIO, c.getMunicipio());
        values.put(DefinirTabla.estados.COLUMN_NAME_CAPITAL, c.getCapital());
        return db.insert(DefinirTabla.estados.TABLE_NAME, null, values);
        //regresa el id insertado
    }
    public long updateEstado(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.estados.COLUMN_NAME_ESTADO, c.getNombre());
        values.put(DefinirTabla.estados.COLUMN_NAME_POBLACION, c.getTamanioPoblacion());
        values.put(DefinirTabla.estados.COLUMN_NAME_ALTITUD, c.getAltitud());
        values.put(DefinirTabla.estados.COLUMN_NAME_MUNICIPIO, c.getMunicipio());
        values.put(DefinirTabla.estados.COLUMN_NAME_CAPITAL, c.getCapital());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.estados.TABLE_NAME , values,
                DefinirTabla.estados._ID + " = " + id,null);
    }
    public int deleteEstado(long id){
        return db.delete(DefinirTabla.estados.TABLE_NAME, DefinirTabla.estados._ID + "=?",
                new String[]{ String.valueOf(id) });
    }
    private Estados readEstado(Cursor cursor){
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTamanioPoblacion(cursor.getString(2));
        c.setAltitud(cursor.getString(3));
        c.setMunicipio(cursor.getString(4));
        c.setCapital(cursor.getString(5));
        return c;
    }
    public Estados getEstado(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.estados.TABLE_NAME,
                columnsToRead,
                DefinirTabla.estados._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados estado = readEstado(c);
        c.close();
        return estado;
    }
    public java.util.ArrayList<Estados> allEstados(){
        Cursor cursor = db.query(DefinirTabla.estados.TABLE_NAME,
                columnsToRead, null, null, null, null, null);
        ArrayList<Estados> estados = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readEstado(cursor);
            estados.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return estados;
    }
    public void close(){
        mDbHelper.close();
    }
}
