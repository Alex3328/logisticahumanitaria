package database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EstadoHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ESTADOS = "CREATE TABLE " +
            DefinirTabla.estados.TABLE_NAME + " ("+
            DefinirTabla.estados._ID + " INTEGER PRIMARY KEY, "+
            DefinirTabla.estados.COLUMN_NAME_ESTADO + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.estados.COLUMN_NAME_POBLACION + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.estados.COLUMN_NAME_ALTITUD + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.estados.COLUMN_NAME_MUNICIPIO + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.estados.COLUMN_NAME_CAPITAL + INTEGER_TYPE +
            ")";

    private static final String SQL_DELETE_ESTADOS = "DROP TABLE IF EXISTS " +
            DefinirTabla.estados.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LogisticaHumanitaria.db";


    public EstadoHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        db.execSQL(SQL_DELETE_ESTADOS);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ESTADOS);
    }
}


