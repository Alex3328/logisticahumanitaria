package database;
import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class estados implements BaseColumns {
        public static final String TABLE_NAME = "estados";
        public static final String COLUMN_NAME_ESTADO = "estado";
        public static final String COLUMN_NAME_POBLACION = "poblacion";
        public static final String COLUMN_NAME_ALTITUD = "altitud";
        public static final String COLUMN_NAME_MUNICIPIO = "municipio";
        public static final String COLUMN_NAME_CAPITAL = "capital";
    }
}
